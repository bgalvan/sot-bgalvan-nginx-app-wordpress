Paso para la instalación de un NGINX PROXY / ACME COMPANION y levantar 2 sitios (APP PELADO/MYSQL y WORDPRESS/MYSQL)

1- Generamos la instancia en AWS con puertos 22/80/443 abiertos 
    
    AMI: Ubuntu 20.04 Server

    Security groups
        SSH 
            Port 22
            Source: Anywhere
        HTTP
            Port 80
            Source: Anywhere
        HTTPS
            Port 443
            Source: Anywhere

2- Generamos 2 sitios en duckdns.org apuntando a la IP PUBLIC de la instancia (uno para app-pelado y otro para wordpress)
3- Clonamos el Repo1 y corremos el init-docker.sh by jorge
4- Instalar Docker-Compose → sudo apt-get install docker-compose
5- Necesitaremos 3 networks las cuales generamos:
    sudo docker network create https-proxy
    sudo docker network create backend-app
    sudo docker network create backend-wordpress
6- Entramos a la carpeta nginx-proxy-acme y hacemos -> sudo docker-compose up -d
7- Entramos a la carpeta app-pelado, editamos los espacios con los dns y email propio y hacemos -> sudo docker-compose up -d
8- Entramos a la carpeta wordpress, editamos los espacios con los dns y email propio y hacemos -> sudo docker-compose up -d





